package com.axual.iris.healthcheck.kafka;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "kubernetes")
@Data
public class KubernetesConfiguration {
    private String namespace;
    private Map<String, RestartServiceCheck> configuration;
    private String tenant;
    private String environment;
    private String stream;
    private String application;
    private String discoveryurl;
    private String certificate;
    private String key;
    private String truststore;
}
