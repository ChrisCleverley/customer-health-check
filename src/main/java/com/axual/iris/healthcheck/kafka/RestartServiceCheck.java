package com.axual.iris.healthcheck.kafka;

import lombok.Data;

@Data
public class RestartServiceCheck {
    private String id;
    private String namespace;
    private String serviceURL;
    private String secret;
    private String deployment;
    private String name;
    private String type;
    private String password;
}
