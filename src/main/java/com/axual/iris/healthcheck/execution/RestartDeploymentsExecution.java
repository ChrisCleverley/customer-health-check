package com.axual.iris.healthcheck.execution;

import com.axual.iris.healthcheck.entities.CertificateDetails;
import com.axual.iris.healthcheck.kafka.RestartServiceCheck;
import com.axual.iris.healthcheck.service.KubernetesSecretService;
import com.axual.iris.healthcheck.utils.CertificateUtils;

import org.apache.commons.io.IOUtils;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1Deployment;
import io.kubernetes.client.openapi.models.V1Secret;
import io.kubernetes.client.openapi.models.V1SecretList;
import io.micrometer.core.instrument.MultiGauge;
import io.micrometer.core.instrument.Tags;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@EnableScheduling
public class RestartDeploymentsExecution {

    private static final long DAY_IN_MS = 1000L * 60 * 60 * 24;
    private final KubernetesSecretService kubernetesSecretService;
    private HashMap<String, Long> expireDates = new HashMap<>();
    private static final String GUESSTHEPASSWORD = "password";

    public RestartDeploymentsExecution(KubernetesSecretService kubernetesSecretService) {
        this.kubernetesSecretService = kubernetesSecretService;
    }

    public void initOnRestart() throws IOException, ApiException {
        V1SecretList secretList = kubernetesSecretService.getListOfSecrets();
        List<String> des = new ArrayList<>();
        for(V1Secret secret : secretList.getItems()){
            Map<String, byte[]> keys = secret.getData();
            String name = secret.getMetadata().getName();
            if (keys != null && keys.size() > 0)
                for (Map.Entry<String, byte[]> oneKey : keys.entrySet()) {
                    Long exptime = expireDates.get(name + "~" + oneKey.getKey());
                    des.addAll(addCertificateToOutputIfNotEmpty(CertificateUtils.getCertDetailsFromSecret(oneKey.getValue(), guessThePassword(secret, oneKey.getKey())),
                            name,
                            oneKey.getKey(),
                            exptime));
                }
        }
        for(String deploymentName: des){
            kubernetesSecretService.restartDeployment(deploymentName);
        }
    }

    private List<String> addCertificateToOutputIfNotEmpty(Optional<CertificateDetails> detailsIfPresent, String secretName, String itemName, Long exptime) {
        List<String> des = new ArrayList<>();
        if (detailsIfPresent.isPresent()) {
            CertificateDetails details = detailsIfPresent.get();
            try {
                String index = secretName + "~" + itemName;
                if (exptime == null) {
                    expireDates.put(index, details.getExpiresValueForPrometheus());
                } else {
                    if (exptime.equals(details.getExpiresValueForPrometheus())) {
                        des.addAll(kubernetesSecretService.getDeploymentsusingSecret(secretName));
                    }
                }
            } catch (RuntimeException rte) {
                log.debug("Unexpected Error While adding row is date empty {}", details.getExpiresDate(), rte);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
        return des;
    }

    @Scheduled(cron = "0 2 * * * *")
    public void runRestartBatch() throws IOException, ApiException{
        initOnRestart();
    }

    private void restartDeployment(String deployment, String namespace) throws IOException, ApiException {
        kubernetesSecretService.restartDeployment(deployment);
    }

    private Date getServiceExpireDate(String service) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        setupConnection();
        URL url = new URL(service);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.connect();
        Certificate[] certs = conn.getServerCertificates();
        for (Certificate c : certs) {
            if (c instanceof  X509Certificate) {
                X509Certificate xc = (X509Certificate) c; // we should really check the type beore doing this typecast..
                return xc.getNotAfter();
            }
        }
        return null;
    }

    private void setupConnection() throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[] { new RemoveSSLCertChecksConnection() };
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }

    private Date getSecretExpireDate(String namespace, String secretName, String secretKey, String password, String typeSecret) throws IOException, ApiException, CertificateException, NoSuchAlgorithmException, KeyStoreException {
        V1Secret secret = kubernetesSecretService.getSecret(namespace, secretName);
        Date expiredDate;
        byte[] secretValue = null;
        Map<String, byte[]> keys = secret.getData();
        if (keys == null || keys.size() == 0)
            throw new FileNotFoundException("Certificate not found in Secret");
        String passwordValue = getPasswordFromSecret(password, keys);
        if (secretName != null) {
            secretValue = keys.get(secretKey);
        }
        if (typeSecret == null || typeSecret.equals("PEM")) {
            expiredDate = getPemExpireDate(secretValue);
        } else {
            expiredDate = getJKSExpiredDate(secretValue, passwordValue);
        }
        return expiredDate;
    }

    private Date getPemExpireDate(byte[] certificate) throws CertificateException {
        Date expiredDate;
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        X509Certificate cer = (X509Certificate) fact.generateCertificate(IOUtils.toInputStream(new String(certificate)));
        expiredDate = cer.getNotAfter();
        return expiredDate;
    }

    private Date getJKSExpiredDate(byte[] secretValue, String passwordValue) throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
        Date expiredDate = null;
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(new ByteArrayInputStream(secretValue), passwordValue.toCharArray());
        for (Iterator<String> it = ks.aliases().asIterator(); it.hasNext(); ) {
            String alias = it.next();
            Certificate cert = ks.getCertificate(alias);
            if (cert instanceof X509Certificate) {
                Date expired = ((X509Certificate)cert).getNotAfter();
                if (expiredDate == null || expiredDate.after(expired)) {
                    expiredDate = expired;
                }
            }
        }
        return expiredDate;
    }

    private String getPasswordFromSecret(String password, Map<String, byte[]> keys){
        String passwordValue = null;
        if (password != null) {
            passwordValue = new String(keys.get(password));
        }
        return passwordValue;
    }

    private Date getTwoDaysFuture() {
        return new Date(System.currentTimeMillis() + (2 * DAY_IN_MS));
    }


    static class RemoveSSLCertChecksConnection implements X509TrustManager {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
            //Do not want validations.
        }
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
            //Do not want validations
        }
    }

    private String guessThePassword(V1Secret secret, String key) {
        if (key.endsWith(".jks")) {
            String guessNumberOne = key.substring(0, key.lastIndexOf('.')) + "-" + GUESSTHEPASSWORD;
            if (secret.getData() != null && secret.getData().containsKey(guessNumberOne)) {
                return new String(secret.getData().get(guessNumberOne));
            }
        }
        String guessNumberTwo = key + "-" + GUESSTHEPASSWORD;
        String guessNumberThree = GUESSTHEPASSWORD;
        if (secret.getData() != null && secret.getData().containsKey(guessNumberTwo)) {
            return new String(secret.getData().get(guessNumberTwo));
        }
        if (secret.getData() != null && secret.getData().containsKey(guessNumberThree)) {
            return new String(secret.getData().get(guessNumberThree));
        }
        return "";
    }
}
