package com.axual.iris.healthcheck.execution;

import com.axual.iris.healthcheck.entities.CertificateDetails;
import com.axual.iris.healthcheck.metrics.CertificateDetailsMetrics;
import com.axual.iris.healthcheck.service.KubernetesSecretService;
import com.axual.iris.healthcheck.utils.CertificateUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1Secret;
import io.kubernetes.client.openapi.models.V1SecretList;
import io.micrometer.core.instrument.MultiGauge;
import io.micrometer.core.instrument.Tags;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@EnableScheduling
public class CertificateDetailsExecution {

    private final CertificateDetailsMetrics certificateDetailsMetrics;
    private final KubernetesSecretService kubernetesSecretService;
    private static final String GUESSTHEPASSWORD = "password";

    @Autowired
    public CertificateDetailsExecution(CertificateDetailsMetrics certificateDetailsMetrics,
                                KubernetesSecretService kubernetesSecretService) {
        this.certificateDetailsMetrics = certificateDetailsMetrics;
        this.kubernetesSecretService = kubernetesSecretService;
    }

    @Scheduled(cron = "0 2 * * * *")
    public void detailsCollection(){
        try {
            log.info("Collcting crtificate data.");
            List<MultiGauge.Row<?>> rows = doCollection();
            certificateDetailsMetrics.getPartitionCounts().register(rows);
            certificateDetailsMetrics.incrementSuccessfulQueries();
        } catch (IOException | ApiException e) {
            log.error("ERROR gathering certificate details", e);
            certificateDetailsMetrics.incrementFailedQueries();
        }
    }

    public List<MultiGauge.Row<?>> doCollection() throws IOException, ApiException {
        V1SecretList secretList = kubernetesSecretService.getListOfSecrets();

        List<MultiGauge.Row<?>> rows = new ArrayList<>();
        for(V1Secret secret : secretList.getItems()){
            Map<String, byte[]> keys = secret.getData();
            String name = secret.getMetadata().getName();
            if (keys != null && keys.size() > 0)
                for (Map.Entry<String, byte[]> oneKey : keys.entrySet()) {
                    addCertificateToOutputIfNotEmpty(CertificateUtils.getCertDetailsFromSecret(oneKey.getValue(), guessThePassword(secret, oneKey.getKey())),
                            rows,
                            name,
                            secret.getMetadata().getNamespace());
                }
        }
        return rows;
    }

    private String guessThePassword(V1Secret secret, String key) {
        if (key.endsWith(".jks")) {
            String guessNumberOne = key.substring(0, key.lastIndexOf('.')) + "-" + GUESSTHEPASSWORD;
            if (secret.getData() != null && secret.getData().containsKey(guessNumberOne)) {
                return new String(secret.getData().get(guessNumberOne));
            }
        }
        String guessNumberTwo = key + "-" + GUESSTHEPASSWORD;
        String guessNumberThree = GUESSTHEPASSWORD;
        if (secret.getData() != null && secret.getData().containsKey(guessNumberTwo)) {
            return new String(secret.getData().get(guessNumberTwo));
        }
        if (secret.getData() != null && secret.getData().containsKey(guessNumberThree)) {
            return new String(secret.getData().get(guessNumberThree));
        }
        return "";
    }

    private void addCertificateToOutputIfNotEmpty(Optional<CertificateDetails> detailsIfPresent, List<MultiGauge.Row<?>> rows, String secretName, String secretNamespace) {
        if (detailsIfPresent.isPresent()) {
            CertificateDetails details = detailsIfPresent.get();
            try {
                rows.add(MultiGauge.Row.of(Tags.of("subject", details.getSubject(), "issuer", details.getIssuer(), "secret", secretName, "namespace", secretNamespace), details.getExpiresValueForPrometheus()));
            } catch (RuntimeException rte) {
                log.debug("Unexpected Error While adding row is date empty {}", details.getExpiresDate(), rte);
            }
        }
    }
}
