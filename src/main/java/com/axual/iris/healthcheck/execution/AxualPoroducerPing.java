package com.axual.iris.healthcheck.execution;

import com.axual.iris.healthcheck.kafka.KubernetesConfiguration;
import com.axual.iris.healthcheck.metrics.ProducerPing;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.CommonClientConfigs.SECURITY_PROTOCOL_CONFIG;
import static org.apache.kafka.common.config.SslConfigs.*;


import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import lombok.extern.slf4j.Slf4j;

import static io.axual.client.proxy.axual.producer.AxualProducerConfig.CHAIN_CONFIG;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;

@Slf4j
@Component
@EnableScheduling
public class AxualPoroducerPing {
    private static String APPLICATION_ID = "io.axual.example.proxy.string.producer";
    private static final String APPLICATION_VERSION = "0.0.1";
    private static final String PEM_TYPE = "PEM";

    private static String STREAM = "string-applicationlog";

    private static String tenant = "axual";
    private static String environment = "example";
    private static String endpoint = "http://127.0.0.1:8081";

    private KubernetesConfiguration kubernetesConfiguration;
    private ProducerPing producerPing;

    @Autowired
    public void AxualPoroducerPing(ProducerPing producerPing, KubernetesConfiguration kubernetesConfiguration){
        this.kubernetesConfiguration = kubernetesConfiguration;
        this.producerPing = producerPing;
        STREAM = kubernetesConfiguration.getStream();
        APPLICATION_ID = kubernetesConfiguration.getApplication();
        endpoint = kubernetesConfiguration.getDiscoveryurl();
        tenant = kubernetesConfiguration.getTenant();
        environment = kubernetesConfiguration.getEnvironment();
    }

    @Scheduled(cron = "* * * * * *")
    public void detailsCollection()  {
        long start = System.nanoTime();
        try {
            Map<String, Object> config = new HashMap<>();
            config.put(CHAIN_CONFIG, ProxyChain.newBuilder()
                    .append(SWITCHING_PROXY_ID)
                    .append(RESOLVING_PROXY_ID)
                    .append(LINEAGE_PROXY_ID)
                    .append(HEADER_PROXY_ID)
                    .build());
            config.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
            config.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);

            config.put(CommonConfig.TENANT, tenant);
            config.put(CommonConfig.ENVIRONMENT, environment);

            config.put(BOOTSTRAP_SERVERS_CONFIG, endpoint);
            config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);

            config.put(SSL_KEYSTORE_TYPE_CONFIG, PEM_TYPE);
            config.put(SSL_TRUSTSTORE_TYPE_CONFIG, PEM_TYPE);

            config.put(SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, kubernetesConfiguration.getCertificate());
            config.put(SSL_KEYSTORE_KEY_CONFIG, new PasswordConfig(kubernetesConfiguration.getKey()));
            config.put(SSL_TRUSTSTORE_CERTIFICATES_CONFIG, kubernetesConfiguration.getTruststore());
            config.put(SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");

            config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, new StringSerializer());
            config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, new StringSerializer());
            config.put(ProducerConfig.ACKS_CONFIG, "1");
            config.put(ProducerConfig.RETRIES_CONFIG, "1");
            config.put(ProducerConfig.LINGER_MS_CONFIG,"0");
            config.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000");
            config.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, "1000");
            config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

            List<Future<RecordMetadata>> futures = new ArrayList<>();
            AxualProducer<String, String> producer = new AxualProducer<>(config);
            producer.send(new ProducerRecord<>(STREAM, "example-key", "example-value" + start)).get();
            producerPing.getMeterRegistry().timer("axual.producer.ping").record(System.nanoTime() - start, TimeUnit.MILLISECONDS);
        } catch(RuntimeException e) {
            producerPing.getMeterRegistry().timer("axual.producer.ping").record(-1l, TimeUnit.MILLISECONDS);
            log.error("Produce Ping failure", e);
        } catch (ExecutionException e) {
            producerPing.getMeterRegistry().timer("axual.producer.ping").record(-1l, TimeUnit.MILLISECONDS);
            log.error("Produce Ping failure", e);
        } catch (InterruptedException e) {
            producerPing.getMeterRegistry().timer("axual.producer.ping").record(-1l, TimeUnit.MILLISECONDS);
            log.error("Produce Ping failure", e);
        }
    }
}
