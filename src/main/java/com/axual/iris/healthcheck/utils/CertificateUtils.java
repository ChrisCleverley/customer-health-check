package com.axual.iris.healthcheck.utils;

import com.axual.iris.healthcheck.entities.CertificateDetails;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CertificateUtils {

    private static final Optional<CertificateDetails> secretNotParsableToACertificate = Optional.empty();

    private CertificateUtils() {}

    public static Optional<CertificateDetails> getCertDetailsFromSecret(byte[] unencodedCert, String password) {
        Optional<CertificateDetails> returnCert = tryToParseAsPEM(unencodedCert);
        if (returnCert.isEmpty()) {
            returnCert = tryToParseAsJKS(unencodedCert, password);
        }
        return returnCert;
    }

    private static Optional<CertificateDetails> tryToParseAsPEM (byte[] unencodedCert) {
        try {
            return Optional.of(getPemExpireDate(unencodedCert));
        } catch (CertificateException e) {
            log.debug("Can not parse PEM", e);
            return secretNotParsableToACertificate;
        }
    }

    private static Optional<CertificateDetails> tryToParseAsJKS (byte[] unencodedCert, String password) {
        try {
            return Optional.of(getJKSExpiredDate(unencodedCert, password));
        } catch (CertificateException e) {
            log.debug("Can parse JKS", e);
            return secretNotParsableToACertificate;
        }
    }



    private static CertificateDetails getPemExpireDate(byte[] certificate) throws CertificateException {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) fact.generateCertificate(IOUtils.toInputStream(new String(certificate)));
        return fillCertDetailsFromX509(cert);
    }

    private static CertificateDetails getJKSExpiredDate(byte[] secretValue, String passwordValue) throws CertificateException {
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(new ByteArrayInputStream(secretValue), passwordValue.toCharArray());
            for (Iterator<String> it = ks.aliases().asIterator(); it.hasNext(); ) {
                String alias = it.next();
                Certificate cert = ks.getCertificate(alias);
                if (cert instanceof X509Certificate) {
                    return fillCertDetailsFromX509((X509Certificate) cert);
                }
            }
        } catch ( KeyStoreException | IOException | NoSuchAlgorithmException ke) {
            log.debug("Keystore exception: ", ke);
        }
        throw new CertificateException("Could not parse certificate") ;
    }

    private static CertificateDetails fillCertDetailsFromX509(X509Certificate certificate) {
        CertificateDetails details = new CertificateDetails();
        details.setIssuer(certificate.getIssuerX500Principal().toString());
        details.setSubject(certificate.getSubjectX500Principal().toString());
        details.setExpiresDate(certificate.getNotAfter());
        return  details;
    }
}
