package com.axual.iris.healthcheck.entities;

import java.util.Date;

import lombok.Data;

@Data
public class CertificateDetails {

    String expires;
    String issuer;
    String subject;
    Date expiresDate;

    public Long getExpiresValueForPrometheus(){
        if (expiresDate == null) {
            return 0L;
        }
        return expiresDate.getTime() / 1000;
    }
}
