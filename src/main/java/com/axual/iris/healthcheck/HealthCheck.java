package com.axual.iris.healthcheck;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class HealthCheck {

    public static void main(String[] args) {
        SpringApplication.run(HealthCheck.class, args);
    }


}
