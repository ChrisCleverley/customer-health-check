package com.axual.iris.healthcheck.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.MultiGauge;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
public class TopicPartitionMetrics {

    private MeterRegistry meterRegistry;
    @Getter
    private MultiGauge partitionCounts;

    public TopicPartitionMetrics(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        partitionCounts = MultiGauge.builder("non_equal_partition_count")
                .register(meterRegistry);
    }


}
