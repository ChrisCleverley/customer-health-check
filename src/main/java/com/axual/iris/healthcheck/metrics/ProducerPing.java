package com.axual.iris.healthcheck.metrics;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.MultiGauge;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProducerPing {

    @Getter
    private final MeterRegistry meterRegistry;

    public ProducerPing(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }
}

