package com.axual.iris.healthcheck.metrics;

import io.micrometer.core.instrument.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

@Slf4j
@Component
public class DistributorMetrics {

    private MeterRegistry meterRegistry;
    private ConcurrentHashMap<String, MultiGauge> listedNotDistedMultiGauges = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, MultiGauge> distedNotListedMultiGauges = new ConcurrentHashMap<>();

    @Autowired
    public DistributorMetrics(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void incrementSuccessfulQueries(Tags tags) {
        Counter.builder("distributor_queries_successful")
                .tags(tags)
                .register(meterRegistry)
                .increment();
    }

    public void incrementFailedQueries(Tags tags) {
        Counter.builder("distributor_queries_failed")
                .tags(tags)
                .register(meterRegistry)
                .increment();
    }

    public void setGauge(String name, Tags tags, long setValue) {
        try {
            AtomicLong value = new AtomicLong(setValue);
            Gauge gauge = Gauge.builder(name, value, AtomicLong::doubleValue)
                    .strongReference(true)
                    .tags(tags)
                    .register(meterRegistry);
        } catch (Throwable t) {
        }
    }

    public void setGauge(String name, Tags tags, Supplier<Number> supplier) {
        try {
            Gauge gauge = Gauge.builder(name, supplier)
                    .strongReference(true)
                    .tags(tags)
                    .register(meterRegistry);
        } catch (Throwable t) {
        }
    }

    public MultiGauge getDistedNotListedMultiGauge(String cluster, String group) {
        return distedNotListedMultiGauges.computeIfAbsent(cluster + group,
                k -> MultiGauge.builder("disted_but_not_listed")
                        .tag("cluster", cluster)
                        .tag("group", group)
                        .register(meterRegistry)
        );
    }

    public MultiGauge getListedNotDistedMultiGauge(String cluster, String group) {
        return listedNotDistedMultiGauges.computeIfAbsent(cluster + group,
                k -> MultiGauge.builder("listed_but_not_disted")
                        .tag("cluster", cluster)
                        .tag("group", group)
                        .register(meterRegistry)
        );
    }

}
