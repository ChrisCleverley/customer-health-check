package com.axual.iris.healthcheck.metrics;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.MultiGauge;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CertificateDetailsMetrics {

    private final MeterRegistry meterRegistry;
    @Getter
    private MultiGauge partitionCounts;

    public CertificateDetailsMetrics(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        partitionCounts = MultiGauge.builder("keystore_ca_certificate_expiry_date_kubernetes")
                .tags(new String[]{"subject", "issuer", "secret", "namespace"})
                .register(meterRegistry);
        log.info("Registered keystore_ca_certificate_expiry_date_kubernetes metric");
    }

    public void incrementSuccessfulQueries() {
        Counter.builder("certificate_queries_successful")
                .register(meterRegistry)
                .increment();
    }

    public void incrementFailedQueries() {
        Counter.builder("certificate_queries_failed")
                .register(meterRegistry)
                .increment();
    }

}

