package com.axual.iris.healthcheck.service;

import com.axual.iris.healthcheck.kafka.KubernetesConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.kubernetes.client.custom.V1Patch;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Deployment;
import io.kubernetes.client.openapi.models.V1DeploymentList;
import io.kubernetes.client.openapi.models.V1Secret;
import io.kubernetes.client.openapi.models.V1SecretList;
import io.kubernetes.client.openapi.models.V1Volume;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.PatchUtils;
import io.kubernetes.client.util.wait.Wait;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KubernetesSecretService {

    KubernetesConfiguration kubernetesConfiguration;

    @Autowired
    public KubernetesSecretService(KubernetesConfiguration kubernetesConfiguration) {
        this.kubernetesConfiguration = kubernetesConfiguration;
    }

    public V1Secret getSecret(String namespace, String name) throws IOException, ApiException {
        ApiClient client = Config.defaultClient();
        CoreV1Api api = new CoreV1Api(client);
        return api.readNamespacedSecret(name, namespace, null);
    }

    public V1SecretList getListOfSecrets() throws IOException, ApiException {
        ApiClient client = Config.defaultClient();
        CoreV1Api api = new CoreV1Api(client);
        return api.listNamespacedSecret(kubernetesConfiguration.getNamespace(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false);

    }

    public List<String> getDeploymentsusingSecret( String secret) throws ApiException, IOException {
        List<String> deploymentUsingSecret = new ArrayList<>();
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        AppsV1Api appsV1Api = new AppsV1Api(client);
        V1DeploymentList runningDeployments =
                appsV1Api.listNamespacedDeployment(kubernetesConfiguration.getNamespace(), null, null, null, null, null, null,null, null, null, null);
        for (V1Deployment runningDeployment: runningDeployments.getItems()) {
            for (V1Volume volume: runningDeployment.getSpec().getTemplate().getSpec().getVolumes())  {
                if (volume.getSecret() !=  null && volume.getSecret().getSecretName().equals(secret))
                    deploymentUsingSecret.add(runningDeployment.getMetadata().getName());
            }
        }
        return deploymentUsingSecret;
    }

    public void restartDeployment(String deploymentName) throws ApiException, IOException {
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        AppsV1Api appsV1Api = new AppsV1Api(client);
        V1DeploymentList runningDeployments =
                appsV1Api.listNamespacedDeployment( kubernetesConfiguration.getNamespace(), null, null, null, null, null, null,null, null, null, null);
        for (V1Deployment runningDeployment: runningDeployments.getItems()) {
            if (runningDeployment.getMetadata().getName().startsWith(deploymentName))  {
                restartOneDeployment(runningDeployment, client, appsV1Api, deploymentName, kubernetesConfiguration.getNamespace());
            }
        }
    }

    private void restartOneDeployment(V1Deployment runningDeployment, ApiClient client, AppsV1Api appsV1Api, String deploymentName, String namespace) {
        runningDeployment
                .getSpec()
                .getTemplate()
                .getMetadata()
                .putAnnotationsItem("kubectl.kubernetes.io/restartedAt", LocalDateTime.now().toString());
        try {
            String deploymentJson = client.getJSON().serialize(runningDeployment);

            PatchUtils.patch(
                    V1Deployment.class,
                    () ->
                            appsV1Api.patchNamespacedDeploymentCall(
                                    deploymentName,
                                    namespace,
                                    new V1Patch(deploymentJson),
                                    null,
                                    null,
                                    "kubectl-rollout",
                                    null,
                                    null,
                                    null),
                    V1Patch.PATCH_FORMAT_STRATEGIC_MERGE_PATCH,
                    client);

            // Wait until deployment has stabilized after rollout restart
            Wait.poll(
                    Duration.ofSeconds(5),
                    Duration.ofSeconds(60),
                    () -> {
                        try {
                            System.out.println("Waiting until example deployment restarted successfully...");
                            return appsV1Api
                                    .readNamespacedDeployment(deploymentName, namespace, null)
                                    .getStatus()
                                    .getReadyReplicas()
                                    > 0;
                        } catch (ApiException e) {
                            e.printStackTrace();
                            return false;
                        }
                    });
            log.info("Example deployment restarted successfully!");
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
}
