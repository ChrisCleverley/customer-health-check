package com.axual.iris.healthcheck.execution;

import com.axual.iris.healthcheck.kafka.KubernetesConfiguration;
import com.axual.iris.healthcheck.metrics.CertificateDetailsMetrics;
import com.axual.iris.healthcheck.service.KubernetesSecretService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import io.kubernetes.client.openapi.ApiException;
import io.micrometer.core.instrument.MultiGauge;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CertificateDetailsExecutionTest {

    @Autowired
    private CertificateDetailsExecution certificateDetailsExecution;

    @Autowired
    private AxualPoroducerPing producerping;

    @Autowired
    private RestartDeploymentsExecution restartDeploymentsExecution;

    @Test
    public void terstProducerCreateRecord(){
        producerping.detailsCollection();
    }

    @Test
    public void terstDeploymentRestart() throws IOException, ApiException {
        restartDeploymentsExecution.runRestartBatch();
        restartDeploymentsExecution.runRestartBatch();
    }

    @Test
    public void testSomeOutputs() throws IOException, ApiException {
//        List<MultiGauge.Row<?>> rows = certificateDetailsExecution.doCollection();
//        assert(rows.size() > 0);
//        for (MultiGauge.Row<?>row: rows) {
//
//            System.out.println(hackToGetWhatIWant(row,"uniqueTags" ) + ":" + hackToGetWhatIWant(row,"obj" ));
//        }
    }

    private String hackToGetWhatIWant(Object obj, String field) {
        try {
            Field f = obj.getClass().getDeclaredField(field); //NoSuchFieldException
            f.setAccessible(true);
            return f.get(obj).toString();
        } catch (Exception e) {
            return "?";
        }
    }
}
