package com.axual.iris.healthcheck.execution;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestartDeploymentsExecutionTest {

    @Autowired
    private RestartDeploymentsExecution restartDeploymentsExecution;

    @Test
    public void findEnddateService() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Date endDate = (Date)hackRunPrivateMethod(restartDeploymentsExecution, "getServiceExpireDate",  "https://platform.local:29000", "kafka", "server-keystore.jks", "server-keystore.password", "JKS", 1);
        assert(endDate).equals(new Date(1794527458000l));
    }

    @Test
    public void findEnddateSecret() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Date endDate = (Date)hackRunPrivateMethod(restartDeploymentsExecution, "getSecretExpireDate", "kafka", "axual-local-discoveryapi-keystore","server-keystore.jks", "server-keystore.password", "JKS", 2);
        assert(endDate).equals(new Date(1794527458000l));
    }

    @Test
    public void enddateSecretEqualsServiceJKS() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Date endDate = (Date)hackRunPrivateMethod(restartDeploymentsExecution, "getServiceExpireDate", "https://platform.local:29000", "kafka", null, null, null,1);
        Date endDateService = (Date)hackRunPrivateMethod(restartDeploymentsExecution, "getSecretExpireDate", "kafka", "axual-local-discoveryapi-keystore","server-keystore.jks", "server-keystore.password", "JKS", 2);
        assert(endDate).equals(endDateService);
    }

    @Test
    public void enddateSecretEqualsServicePEM() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Date endDateService = (Date)hackRunPrivateMethod(restartDeploymentsExecution, "getSecretExpireDate", "kafka", "axual-local-discoveryapi-keystore","local-axual-deploy-certs", null, "PEM", 2);
        assert(endDateService).equals(new Date(1794527458000l));
    }

    @Test
    public void resttartdisco() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Date endDateService = (Date)hackRunPrivateMethod(restartDeploymentsExecution, "restartDeployment", "axual-local-discovery-api","kafka","local-axual-deploy-certs", null, "PEM", 3);
        assert(true);
        //Manual integration test.
    }

    private Object hackRunPrivateMethod(Object o, String name, String namespace, String service, String secretKey, String passwordKey, String certtype,int howmany) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Object returnValue = null;
        Method method;
        switch (howmany) {
            case 1: {
                method = o.getClass().getDeclaredMethod(name, String.class);
                method.setAccessible(true);
                returnValue = method.invoke(o, namespace);
            }
            break;
            case 2:
            {
                method = o.getClass().getDeclaredMethod(name, String.class, String.class, String.class, String.class, String.class);
                method.setAccessible(true);
                returnValue = method.invoke(o, namespace, service, secretKey, passwordKey, certtype);
            }
            break;
            case 3:
            {
                method = o.getClass().getDeclaredMethod(name, String.class, String.class);
                method.setAccessible(true);
                returnValue = method.invoke(o, namespace, service);
            }
        }
        return returnValue;
    }
}
