FROM openjdk:11-slim
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} iris-health-check-0.2.0-snapshot.jar
ENTRYPOINT ["java","-jar","/iris-health-check-0.2.0-snapshot.jar"]